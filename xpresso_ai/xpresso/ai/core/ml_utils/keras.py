import types

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.utils import multiclass
from tensorflow.keras.losses import \
    MeanAbsolutePercentageError, \
    CosineSimilarity, Huber, LogCosh, BinaryCrossentropy, Poisson, Hinge, \
    SquaredHinge, CategoricalCrossentropy, KLDivergence, CategoricalHinge
from tensorflow.keras.metrics import TruePositives, TrueNegatives, \
    FalsePositives, FalseNegatives, PrecisionAtRecall, \
    SensitivityAtSpecificity, SpecificityAtSensitivity
from tensorflow.python.keras.losses import SparseCategoricalCrossentropy
from xpresso.ai.core.commons.utils.constants import EMPTY_STRING
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.ml_utils.generic import REGRESSOR, CLASSIFIER, \
    CLASSIFIER_TARGET_TYPES, BINARY, PLOTS_FOLDER
from xpresso.ai.core.ml_utils.sklearn import SklearnMetrics
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

KERAS_REGRESSOR_METRICS = [
    MeanAbsolutePercentageError(), CosineSimilarity(),
    Huber(), LogCosh()]
KERAS_BINARY_METRICS = [
    TruePositives(), TrueNegatives(), FalsePositives(), FalseNegatives(),
    PrecisionAtRecall(0.5), SensitivityAtSpecificity(0.5),
    SpecificityAtSensitivity(0.5), BinaryCrossentropy(), Poisson(), Hinge(),
    SquaredHinge()]
KERAS_MULTILABEL_METRICS = [
    CategoricalCrossentropy(), Poisson(), KLDivergence(), Hinge(),
    SquaredHinge(), CategoricalHinge()]
KERAS_MULTICLASS_METRICS = [SparseCategoricalCrossentropy()]

class KerasPlots:
    def __init__(self):
        self.logger = XprLogger(name="KerasPlots")

    def plot_learning_curve(self, history=None):
        """
        Generate learning curves(the test, train scores vs epoch) with the
        saved history of training Keras model.
        Args:
            history: A History object returned from Keras model.fit()
        Returns: Learning curve
        """
        if not history:
            return
        # get keys in training history
        hist_keys = list(history.history.keys())
        # unique metrics in training history
        metrics_list = [key for index, key in enumerate(hist_keys)
                        if index < len(hist_keys) / 2]
        # number of metrics
        n_metrics = len(metrics_list)
        # create subplots
        fig, axes = plt.subplots(n_metrics, 1, figsize=(6, 8))
        # plot learning curve for each scoring metrics
        for i in range(n_metrics):
            try:
                # get training and validation scores for the current metric
                results = pd.DataFrame(history.history).iloc[:,
                          [i, i + n_metrics]]

                # collect number of epoch
                epoch_range = list(range(1, results.shape[0] + 1))

                # identify best epoch and the score
                if ("loss" in metrics_list[i]) or ("error" in metrics_list[i]):
                    val_score_best = results.iloc[:, 1].min()
                    max_iters_best = results.iloc[:, 1].idxmin() + 1
                else:
                    val_score_best = results.iloc[:, 1].max()
                    max_iters_best = results.iloc[:, 1].idxmax() + 1
                # Plot score vs max_iters
                axes[i].grid()
                axes[i].plot(
                    epoch_range, results.iloc[:, 0], 'o-', color="y",
                    label=results.columns[0])
                axes[i].plot(
                    epoch_range, results.iloc[:, 1], 'o-', color="g",
                    label=results.columns[1])
                axes[i].plot(
                    max_iters_best, val_score_best, 'r*',
                    label='Best epoch : ' + str(max_iters_best))
                axes[i].set_xlabel("epoch")
                axes[i].set_ylabel(metrics_list[i])
                axes[i].set_title("Keras Learning Curve :" + metrics_list[i])
                axes[i].legend(loc="best")
            except Exception as e:
                self.logger.exception("Unable to plot learning_curve")
        fig.tight_layout(pad=3.0)
        xpresso_save_plot(f"keras_learning_curve", output_path=PLOTS_FOLDER)


class KerasMetrics(SklearnMetrics):
    def __init__(self, model, parameters_json=dict()):
        super().__init__(model, parameters_json)

    def validate_model_type(self):
        if not isinstance(self.model, tf.keras.Model):
            self.logger.info("Invalid model, model not of type "
                             "tensorflow.keras")
            return False
        return True

    def set_model_type(self, y_values, model_type=EMPTY_STRING):
        """
        Identify model type (regressor or classifier) based on user's input,
            model loss or data type of the target variable
        Determine the model is a regressor or classifier.
        If the user provides True/False to is_regressor parameter,
        apply user's input. If not, first infer model type
        based on the model loss used in training . If model loss is not
        clear, then use Sklearn type_of_target
        method to identify data type of target variable.
        """
        regression_losses = ["mean_squared_error", "mse",
                             "mean_absolute_error", "mae",
                             "mean_absolute_percentage_error", "mape",
                             "mean_squared_logarithmic_error", "msle",
                             "cosine_similarity", "cosine",
                             "huber_loss",
                             "log_cosh"]

        probabilistic_losses = ["binary_crossentropy",
                                "categorical_crossentropy",
                                "sparse_categorical_crossentropy",
                                "poisson",
                                "KLDivergence",
                                "hinge",
                                "squared_hinge",
                                "categorical_hinge"]

        if model_type and model_type.lower() in [REGRESSOR, CLASSIFIER]:
            self.model_type = model_type
        # determine model type based on model loss
        elif self.model.loss:
            model_loss = EMPTY_STRING
            if isinstance(self.model.loss, str):
                model_loss = self.model.loss
            elif isinstance(self.model.loss, types.FunctionType):
                model_loss = self.model.loss.__name__
            elif isinstance(self.model.loss, tf.keras.losses.Loss):
                model_loss = self.model.loss.__class__.__name__
            if model_loss in regression_losses:
                self.model_type = REGRESSOR
            elif model_loss in probabilistic_losses:
                self.model_type = CLASSIFIER
        # if model loss is unclear, infer model type based on type of
        # target variable
        else:
            self.set_target_type(y_values)
            if self.target_type.startswith("continuous"):
                self.model_type = REGRESSOR
            elif self.target_type in CLASSIFIER_TARGET_TYPES:
                self.model_type = CLASSIFIER

    def predict_and_evaluate(self, x_values, y_values, prediction_threshold,
                             dataset_name=EMPTY_STRING,
                             model_type=EMPTY_STRING):
        self.metric_suffix = dataset_name

        # identify model_type
        self.set_model_type(y_values, model_type)

        # identify target_type
        self.set_target_type(y_values)

        # make predictions
        y_pred, y_pred_proba = self.generate_prediction(x_values, y_values)
        y_pred_label = self.get_y_pred_label(y_pred, prediction_threshold)

        # populate metrics
        self.populate_metrics(x_values, y_values, y_pred, y_pred_proba,
                              y_pred_label)

    def get_y_pred_label(self, y_pred, prediction_threshold):
        if self.target_type.startswith("multiclass"):
            y_pred_label = np.argmax(y_pred, axis=1)
        else:
            y_pred_label = (y_pred > prediction_threshold).astype(int)
        return y_pred_label

    def generate_prediction(self, x_values, y_values):

        """
        Generate prediction using model.predict()

        Returns: Predictions, the output from running Keras model.predict() (
        labeled as y_pred).
                For Keras classification models, calling model.predict()
                outputs prediction probabilities.
                Generate y_pred_label by comparing prediction probabilities
                with threshold (default 0.5)
        """
        print("Generating predictions for given dataset")
        try:
            y_pred_proba = None

            # Apply trained model to make predictions on given dataset
            y_pred = self.model.predict(x_values)
            y_pred = y_pred.astype("float64")

            # for binary classification model
            if self.target_type == BINARY:
                y_pred_proba = np.c_[1 - y_pred, y_pred]
            # for multiclass
            elif self.target_type.startswith("multiclass") or \
                (not multiclass.is_multilabel(y_values)):
                y_pred_proba = y_pred
            # user uses CategoricalCrossentropy (y labels using one-hot
            # representation)
            elif self.target_type.startswith(
                "multilabel") or multiclass.is_multilabel(y_values):
                y_pred_proba = y_pred
            return y_pred, y_pred_proba
        except Exception as exp:
            self.logger.exception(
                f"Unable to generate predictions. Reason:{exp}")

    def populate_metrics(self, x_values, y_true, y_pred, y_pred_proba,
                         y_pred_label):
        """
        Returns: populate metrics based on model type (regressor or classifier)
        """
        print("Populating metrics")

        if self.model_type == REGRESSOR:
            super().populate_metrics_regressor(y_true, y_pred_label)
            self.populate_metrics_regressor(y_true, y_pred)
        elif self.model_type == CLASSIFIER:
            self.populate_metrics_classifier(y_true, y_pred, y_pred_proba,
                                             y_pred_label)

    def populate_metrics_regressor(self, y_true, y_pred):
        """
        Populate metrics for regression models using sklearn and Keras
        metrics APIs

        Returns: metrics values and metrics name

        """
        print("Populating regressor metrics keras")
        self.calculate_metrics(KERAS_REGRESSOR_METRICS, y_true, y_pred)

    def populate_metrics_classifier(self, y_true, y_pred, y_pred_proba,
                                    y_pred_label):
        """
        Populate metrics, save classification report (as csv file) and plot
        confusion matrix, roc curve and
        precision and recall curve for all three type of classifiers ((
        binary, multiclass or multilable).
        KerasComponent leverage metrics available from sklearn and Keras
        library. The metrics that will be
        finally reported depends on the classifier type (binary, multiclass
        or multilabel)
        Returns: metrics values and metrics name
        """
        print("Populating classifier metrics")
        if self.target_type not in CLASSIFIER_TARGET_TYPES:
            return

        super().populate_metrics_classifier(y_true, y_pred_label,
                                            y_pred_proba, None)

        if self.target_type == BINARY:
            self.calculate_metrics(KERAS_BINARY_METRICS, y_true, y_pred)
        elif multiclass.unique_labels(
            y_true).size > 2 and multiclass.is_multilabel(y_true):
            self.calculate_metrics(KERAS_MULTILABEL_METRICS, y_true,y_pred)
        elif multiclass.unique_labels(y_true).size > 2:
            self.calculate_metrics(KERAS_MULTICLASS_METRICS, y_true, y_pred)
