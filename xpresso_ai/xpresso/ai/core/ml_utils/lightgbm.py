import lightgbm as lgb

from xpresso.ai.core.commons.utils.constants import EMPTY_STRING, \
    DEFAULT_VALIDATION_SIZE, DEFAULT_PREDICTION_THRESHOLD
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.ml_utils.generic import REGRESSOR, CLASSIFIER, \
    CLASSIFIER_TARGET_TYPES
from xpresso.ai.core.ml_utils.sklearn import SklearnMetrics


class LightgbmMetrics(SklearnMetrics):
    def __init__(self, model):
        super().__init__(model)

    def validate_model_type(self):
        if not (isinstance(self.model, lgb.Booster) or
                isinstance(self.model, lgb.sklearn.LGBMModel)):
            return False
        return True

    def get_all_metrics(self, x_values, y_values,
                        model_type=EMPTY_STRING,
                        validation_size=DEFAULT_VALIDATION_SIZE,
                        generate_validation_metrics=False,
                        prediction_threshold=DEFAULT_PREDICTION_THRESHOLD):
        self.is_classifier = is_classifier
        self.is_regressor = None
        self.pred_thld = pred_thld

        # identify model type (regressor or classifier) based on user's input
        # or the data type of target variable
        self.identify_model_type()

        # initiate two empty lists for collecting metrics values and names
        self.metrics_value = []
        self.metrics_name = []

        # report best iteration for model training process
        self.report_training_best_iteration()

        # datatype check : if model is LightGBM model trained with
        # Scikit-Learn API
        if isinstance(self.model, lgb.sklearn.LGBMModel):
            # plot learning curve of the model training process
            self.plot_learning_curve(model=self.model)

        # if validation = True, create train and validation sets from X,
        # y dataset provided
        if generate_validation_metrics:
            X_train, X_val, y_train, y_val = train_test_split(X, y,
                                                              test_size=validation_size,
                                                              random_state=42)

            # generate metrics for training and validation set separately
            for x, y, dname in [(X_train, y_train, " (Tr)"),
                                (X_val, y_val, " (Val)")]:
                # assign predictors based on model class (LightGBM booster or
                # LightGBM sklearn api model)
                self.predict_and_evaluate(x, y, dname)

        # if validation = False
        else:
            dname = ""
            self.predict_and_evaluate(X, y, dname)

        # collect metrics names and values in a dictionary
        self.metrics_dict = dict(zip(self.metrics_name, self.metrics_value))

        # send metrics to xpresso UI
        self.send_metrics(metrics=self.metrics_dict,
                          status_desc=self.status_desc)

        logger.info("Metrics Reported")

    def predict_and_evaluate(self, x, y, dname):
        """

        Args:
            x: array_like or sparse matrix, shape (n_samples, n_features)
                input matrix (either training set, validation set, or test set)
            y: array-like of shape (n_samples,) or (n_samples, n_classes)
                target variable vector or matrix (either training set,
                validation set, or test set)
            dname: string
                 A label indicating the dataset name, automaticall assigned
                 by LightGBMComponent.
                The value will be "tr" and "val", indicating training set,
                validation set, if generate_validation_metrics
                 is True. Otherwise, the value will be "" if
                 generate_validation_metrics is False or the data type is
                 not able to be split into training and validation set with
                 sklearn train_test_split.

        Returns: metrics values(list) and metrics names(list)

        """
        # assign independent variables
        self.X = x
        # assign target variable
        self.y_true = y
        # assign dataset name
        self.dataset_name = dname

        # make predictions
        self.generate_prediction()

        # populate metrics
        self.populate_metrics()

    def set_model_type(self, y_values, model_type=EMPTY_STRING):
        """
                Determine the model is a regressor or classifier.
                If user provides True/False to is_regressor parameter,
                apply user's
                input. If not, infer model type
                based on the data type of target variable with Sklearn
                type_of_target
                method

                """

        lgb_objectives_regr = ["regression", "regression_l2", "l2",
                               "mean_squared_error", "mse", "l2_root",
                               "root_mean_squared_error",
                               "root_mean_squared_error",
                               "regression_l1", "l1", "mean_absolute_error",
                               "mae",
                               "mean_absolute_error", "fair", "poisson",
                               "quantile",
                               "mape", "mean_absolute_percentage_error",
                               "gamma", "tweedie"]
        lgb_objectives_clf = ["binary",
                              "multiclass",
                              "multiclassova",
                              "cross_entropy",
                              "xentropy",
                              "cross_entropy_lambda",
                              "xentlambda"]

        lgb_metrics_regr = ["l1", "mean_absolute_error", "mae", "l2",
                            "mean_squared_error", "mse", "regression_l2",
                            "regression",
                            "rmse", "root_mean_squared_error", "l2_root",
                            "quantile", "mape",
                            "mean_absolute_percentage_error",
                            "huber", "fair", "poisson", "gamma",
                            "gamma_deviance", "tweedie"]

        lgb_metrics_clf = ["map", "mean_average_precision", "auc",
                           "binary_logloss", "binary", "binary_error",
                           "auc_mu", "multi_logloss", "multiclass", "softmax",
                           'multiclassova', "multiclass_ova", "ova", "ovr",
                           "multi_error", "cross_entropy", "xentropy",
                           "cross_entropy_lambda", "xentlambda",
                           "kullback_leibler", "kldiv"]

        if model_type and model_type.lower() in [REGRESSOR, CLASSIFIER]:
            self.model_type = model_type
        # if user doesn't specify model type, try identify model type from
        # model objective or evaluation metrics
        # from the parameters of model
        else:
            params_metrics, params_objective = \
                self.identify_params_obj_metrics()
            # first try identifying model type from model objective
            if params_objective:
                try:
                    if params_objective in lgb_objectives_clf:
                        self.model_type = CLASSIFIER
                    elif params_objective in lgb_objectives_regr:
                        self.model_type = REGRESSOR
                except Exception:
                    self.logger.exception("Unable to determine model type "
                                          "from losses")
            # then try identifying model type from model evaluation metrics
            elif params_metrics:
                try:
                    if any(mtr in lgb_metrics_clf for mtr in params_metrics):
                        self.model_type = CLASSIFIER
                    elif any(mtr in lgb_metrics_regr for mtr in params_metrics):
                        self.model_type = REGRESSOR
                except Exception:
                    self.logger.exception("Unable to determine model type "
                                          "from losses")
        # finally use sklearn type_of_target method to identify data type of
        # target variable
        if self.model_type not in [CLASSIFIER, REGRESSOR]:
            self.set_target_type(y_values)
            if self.target_type.startswith("continuous"):
                self.model_type = REGRESSOR
            elif self.target_type in CLASSIFIER_TARGET_TYPES:
                self.model_type = CLASSIFIER

    def identify_params_obj_metrics(self):
        """
        Identify objectives and evaluation metrics from the model parameters

        """
        params_metrics = None
        params_objective = None
        if isinstance(self.model, lgb.Booster):
            try:
                params_objective = self.model.params['objective']
                params_metrics = self.model.params['metric']
            except Exception as exp:
                self.logger.exception(f"Reason: {exp}")

        elif isinstance(self.model, lgb.sklearn.LGBMModel):
            try:
                params_objective = self.model.objective_
                # collect name of evaluation metrics from model.evals_results_
                params_metrics = []
                for val_sets, e_mtrs in self.model.evals_result_.items():
                    for e_mtr_name, e_mtr_vals in e_mtrs.items():
                        params_metrics.append(e_mtr_name)
            except Exception as exp:
                self.logger.exception(f"Reason: {exp}")
        return params_metrics, params_objective

    def identify_model_classes(self):
        """

        Identify the classes/labels for the target variables in case of a
        classification model

        """
        # identify model classes
        if isinstance(self.model, lgb.sklearn.LGBMModel):
            try:
                self.model_classes = self.model.classes_
            except Exception:
                import traceback
                traceback.print_exc()
        else:
            try:
                self.model_classes = sklearn.utils.multiclass.unique_labels(
                    self.y_true)
            except Exception:
                import traceback
                traceback.print_exc()

    def binary_or_multiclass(self):
        """
        Determine the model is a regressor or classifier.
        If user provides True/False to is_regressor parameter, apply user's
        input. If not, infer model type
        based on the data type of target variable with Sklearn type_of_target
        method

        """

        # identify model classes
        self.identify_model_classes()

        lgb_metrics_binary = ["binary_logloss", "binary", "binary_error"]
        lgb_metrics_multicls = ["auc_mu", "multi_logloss", "multiclass",
                                "softmax",
                                'multiclassova', "multiclass_ova", "ova", "ovr",
                                "multi_error"]

        # identify model objective and evaluation metrics
        params_metrics, params_objective = self.identify_params_obj_metrics()

        # identify classification type using model objective
        if params_objective:
            try:
                self.is_binary = params_objective == "binary"
                self.is_multicls = params_objective in ["multiclass",
                                                        "multiclassova"]
                if (type(self.is_binary) == bool) & (
                    type(self.is_multicls) == bool) & \
                    (self.is_binary != self.is_multicls):
                    return
            except Exception:
                import traceback
                traceback.print_exc()

        # identify classification type using model evaluation metrics
        elif params_metrics:
            try:
                self.is_binary = any(
                    mtr in lgb_metrics_binary for mtr in params_metrics)
                self.is_multicls = any(
                    mtr in lgb_metrics_multicls for mtr in params_metrics)
                if (type(self.is_binary) == bool) & (
                    type(self.is_multicls) == bool) & \
                    (self.is_binary != self.is_multicls):
                    return
            except Exception:
                import traceback
                traceback.print_exc()

        elif self.model_classes.any():
            try:
                self.is_binary = self.model_classes.size == 2
                self.is_multicls = self.model_classes.size > 2
                if (type(self.is_binary) == bool) & (
                    type(self.is_multicls) == bool) & \
                    (self.is_binary != self.is_multicls):
                    return
            except Exception:
                import traceback
                traceback.print_exc()
        else:
            try:
                target_type_multicls_classifier = ["multiclass",
                                                   "multiclass-multioutput",
                                                   "multilabel-indicator"]

                self.target_type = sklearn.utils.multiclass.type_of_target(
                    self.y_true)
                self.is_binary = self.target_type == "binary"
                self.is_multicls = self.target_type in \
                                   target_type_multicls_classifier

                if (type(self.is_binary) == bool) & (
                    type(self.is_multicls) == bool) & \
                    (self.is_binary != self.is_multicls):
                    return
            except Exception:
                import traceback
                traceback.print_exc()

    def report_training_best_iteration(self):
        """

        Identify and report best iteration and best score during LightGBM
        model training, support when the model is
        trained with LightGBM Scikit-Learn API

        """
        # datatype check : if model is a LightGBM model trained with
        # Scikit-Learn API
        if isinstance(self.model, lgb.sklearn.LGBMModel):
            try:
                for k, v in self.model.best_score_.items():
                    for m_name, m_value in v.items():
                        self.metrics_name.append(
                            "Best Score (" + self.reformat_metrics(
                                m_name) + ") During Training")
                        self.metrics_value.append(round(m_value, 2))
            except Exception:
                import traceback
                traceback.print_exc()

            try:
                self.metrics_value.append(self.model.best_iteration_)
                self.metrics_name.append("Best Iteration During Training")
            except Exception:
                import traceback
                traceback.print_exc()

    def generate_prediction(self):
        """
        Generate prediction using model.predict()

        Returns: Prediction, the ooutput from running LightGBM model.predict(
        ) (labeled as y_pred).
                Additional, predicted probabilities if the classification
                model has attribute of predict_proba.

        """
        try:
            # Apply trained model to make predictions on given dataset
            self.y_pred = self.model.predict(self.X)

            # Check if the trained model can predict probability
            if hasattr(self.model, "predict_proba"):
                self.y_pred_proba = self.model.predict_proba(self.X)
            else:
                self.y_pred_proba = None

            classifier_target_type = ["binary", "multiclass",
                                      "multiclass-multioutput",
                                      "multilabel-indicator"]
            # if objective is binary:logistic, the output of model.predict()
            # is probabilities not the exact label
            if (type_of_target(self.y_true) in classifier_target_type) and \
                (type_of_target(self.y_pred).startswith("continuous")):

                self.y_pred_proba = self.y_pred
                self.y_pred = (self.y_pred > self.pred_thld).astype(int)

                # reshape y_pred_proba array
                if type_of_target(self.y_true) in ["binary"]:
                    self.y_pred_proba = np.c_[1 - self.y_pred, self.y_pred]

        except Exception as e:
            print(e)
            pass

    def populate_metrics(self):
        """

        Returns: populate metrics based on model type (regressor or classifier)

        """
        # create a folder to save images or csv files generated from calling
        # report metrics method
        report_metrics_path = MOUNT_PATH + "/report_metrics"
        if not os.path.exists(report_metrics_path):
            os.makedirs(report_metrics_path)

        # check if the model is lightgbm regressor
        if isinstance(self.model, lgb.LGBMRegressor) or self.is_regressor:
            self.populate_metrics_regressor()

        # check if the model is a lightgbm classifier
        elif isinstance(self.model, lgb.LGBMClassifier) or self.is_classifier:
            self.populate_metrics_classifier()

    def calculate_metrics_general(self, sklearn_metrics):
        """
        Calculate and record metrics available in sklearn library using
        y_true and y_pred as inputs
        Args:
            sklearn_metrics: sklearn metrics APIs, automatically assigned by
            LightGBMComponent based on model type.

        Returns:
            metrics values and metrics name

        """
        for metric in sklearn_metrics:
            try:
                self.metrics_value.append(
                    round(metric(self.y_true, self.y_pred), 2))
                self.metrics_name.append(
                    self.get_name(metric) + self.dataset_name)
            except Exception as e:
                print(e)
                pass

    def populate_metrics_regressor(self):
        """
        Populate metrics for regression models using sklearn metrics APIs

        Returns: metrics values and metrics name

        """
        # collect sklearn regression metrics
        sklearn_metrics = [metrics.mean_absolute_error,
                           metrics.mean_squared_error,
                           metrics.mean_squared_log_error,
                           metrics.median_absolute_error,
                           metrics.explained_variance_score,
                           metrics.max_error,
                           metrics.r2_score,
                           metrics.mean_poisson_deviance,
                           metrics.mean_gamma_deviance,
                           metrics.mean_tweedie_deviance]

        self.calculate_metrics_general(sklearn_metrics)

    def populate_metrics_classifier(self):
        """
        Populate metrics, save classification report (as csv file) and plot
        confusion matrix, roc curve and
        precision and recall curve for all three type of classifiers ((
        binary, multiclass or multilable).
        LightGBMComponent leverage metrics available from sklearn library.
        The metrics that will be
        finally reported depends on the classifier type (binary, multiclass
        or multilable)

        Returns: metrics values and metrics name

        """

        # identify model classes
        self.identify_model_classes()

        if len(self.model_classes) < 2:
            return

        # identify model is a binary or multiclass
        self.binary_or_multiclass()

        # save classification report for all types
        self.save_classification_report()

        # Calculate metrics that use y_true and y_pred_proba as inputs
        if self.y_pred_proba is not None:
            proba_metrics = [metrics.log_loss]
            self.populate_metrics_yproba(proba_metrics)

        # check if the model is binary classifier:
        if self.is_binary:

            self.populate_metrics_binary()

            # plot confusion matrix for binary or multiclass classification task
            self.plot_confusion_matrix()

            if self.y_pred_proba is None:
                return

            # plot roc curve for binary classifier
            self.plot_roc_curve_binary()

            # plot precision_recall_curve for binary classifier
            self.plot_precision_recall_curve_binary()

        # check if the model is a multiclass or multilabel classifier
        elif self.is_multicls:

            # plot roc curve for multiclass or multilabels
            self.plot_roc_curve_multi()

            # plot precision recall curve for multiclass or multilabels
            self.plot_precision_recall_curve_multi()

            # for multiclass classification
            if not sklearn.utils.multiclass.is_multilabel(self.y_true):
                self.populate_metrics_multiclass()

                # plot confusion matrix for binary or multiclass
                # classification task
                self.plot_confusion_matrix()

            # for multilabel classification
            elif sklearn.utils.multiclass.is_multilabel(self.y_true):
                self.populate_metrics_multilabel()

                # plot multilabel confusion matrix for all types of
                # classification task
                self.plot_multilabel_confusion_matrix()

    def populate_metrics_yproba(self, proba_metrics):
        """
        Populate metrics that use y_true and y_pred_proba (predicted
        probability) as inputs

        Args:
            proba_metrics: list of sklearn metrics APIs

        Returns: metrics values and metrics name

        """
        for metric in proba_metrics:
            try:
                self.metrics_value.append(
                    round(metric(self.y_true, self.y_pred_proba), 4))
                self.metrics_name.append(
                    self.get_name(metric) + self.dataset_name)
            except Exception as e:
                print(e)
                pass

    def populate_metrics_binary(self):
        """
        Populate metrics for binary classifiers using both sklearn metrics APIs.

        Returns: metrics values and metrics name

        """
        # metrics using y_true and y_pred as inputs
        sklearn_metrics = [sklearn.metrics.accuracy_score,
                           sklearn.metrics.balanced_accuracy_score,
                           sklearn.metrics.recall_score,
                           sklearn.metrics.precision_score,
                           sklearn.metrics.f1_score,
                           sklearn.metrics.zero_one_loss,
                           sklearn.metrics.hamming_loss,
                           sklearn.metrics.jaccard_score,
                           sklearn.metrics.matthews_corrcoef]

        self.calculate_metrics_general(sklearn_metrics)

        # calculate metrics that use prediction probabilities as inputs
        if self.y_pred_proba is not None:
            try:
                fpr, tpr, thresholds = sklearn.metrics.roc_curve(self.y_true,
                                                                 self.y_pred_proba[
                                                                 :, 1])
                self.metrics_value.append(
                    round(sklearn.metrics.auc(fpr, tpr), 2))
                self.metrics_name.append(
                    self.get_name(sklearn.metrics.auc) + self.dataset_name)
            except Exception as e:
                print(e)
                raise

            clf_metrics_proba = [metrics.roc_auc_score,
                                 metrics.average_precision_score,
                                 metrics.brier_score_loss]
            # calculate metrics that use y_true and y_pred_proba as inputs
            for metric in clf_metrics_proba:
                try:
                    self.metrics_value.append(
                        round(metric(self.y_true, self.y_pred_proba[:, 1]), 2))
                    self.metrics_name.append(
                        self.get_name(metric) + self.dataset_name)
                except Exception as e:
                    print(e)
                    pass

        # add additional metrics from confusion matrix
        try:
            tn, fp, fn, tp = metrics.confusion_matrix(self.y_true,
                                                      self.y_pred).ravel()
            self.metrics_value.extend([self.true_negative_rate(tn, fp),
                                       self.false_positive_rate(fp, tn),
                                       self.false_negative_rate(fn, tp)])
            self.metrics_name.extend(
                [self.get_name(self.true_negative_rate) + self.dataset_name,
                 self.get_name(self.false_positive_rate) + self.dataset_name,
                 self.get_name(self.false_negative_rate) + self.dataset_name])
        except Exception as e:
            print(e)
            pass

    def true_negative_rate(self, tn, fp):
        """
        Args:
            tn: True Negatives (count)
            fp: False Positives (count)

        Returns: true_negative_rate
        """
        return round(tn / (tn + fp), 2)

    def false_positive_rate(self, fp, tn):
        """
        Args:
            fp: False Positives (count)
            tn: True Negatives (count)

        Returns: false_positive_rate
        """
        return round(fp / (fp + tn), 2)

    def false_negative_rate(self, fn, tp):
        """
        Args:
            fn: False Negatives (count)
            tp: True Positives (count)

        Returns: false_negative_rate
        """
        return round(fn / (fn + tp), 2)

    def populate_metrics_multiclass(self):
        """

        Populate metrics for multiclass classifiers using both sklearn
        metrics APIs

        Returns: metrics values and metrics name

        """

        sklearn_metrics = [metrics.accuracy_score,
                           metrics.zero_one_loss,
                           metrics.hamming_loss,
                           metrics.balanced_accuracy_score,
                           metrics.matthews_corrcoef]
        self.calculate_metrics_general(sklearn_metrics)

        #  metrics that requires averaging method
        averaging_metrics = [metrics.f1_score,
                             metrics.recall_score,
                             metrics.precision_score,
                             metrics.jaccard_score]

        for metric in averaging_metrics:
            for avg_method in ["micro", "macro", "weighted"]:
                try:
                    self.metrics_value.append(round(
                        metric(self.y_true, self.y_pred, average=avg_method),
                        4))
                    self.metrics_name.append(self.get_name(
                        metric) + "_" + avg_method + self.dataset_name)
                except Exception as e:
                    print(e)
                    pass

        # roc_auc_score
        if self.y_pred_proba is not None:
            for avg_method, config in itertools.product(["macro", "weighted"],
                                                        ["ovr", "ovo"]):
                try:
                    self.metrics_value.append(round(
                        metrics.roc_auc_score(self.y_true, self.y_pred_proba,
                                              average=avg_method,
                                              multi_class=config), 4))
                    self.metrics_name.append(self.get_name(
                        metrics.roc_auc_score) + "_" + avg_method + "_" + config
                                             + self.dataset_name)

                except Exception as e:
                    print(e)
                    pass

    def populate_metrics_multilabel(self):
        """

        Populate metrics for multilabel classifiers with both sklearn metrics
        APIs

        Returns: metrics values and metrics name

        """

        # collect and calculate metrics that use y_true, y_pred as inputs
        sklearn_metrics = [metrics.accuracy_score,
                           metrics.zero_one_loss,
                           metrics.hamming_loss,
                           metrics.average_precision_score]

        self.calculate_metrics_general(sklearn_metrics)

        # Calculate metrics that use y_true and y_pred_proba as inputs
        if self.y_pred_proba is not None:
            proba_metrics = [metrics.log_loss]
            self.populate_metrics_yproba(proba_metrics)

        # averaging metrics
        averaging_metrics = [metrics.f1_score,
                             metrics.recall_score,
                             metrics.precision_score,
                             metrics.jaccard_score]

        for metric in averaging_metrics:
            for avg_method in ["micro", "macro", "weighted", "samples"]:
                try:
                    self.metrics_value.append(round(
                        metric(self.y_true, self.y_pred, average=avg_method),
                        4))
                    self.metrics_name.append(self.get_name(
                        metric) + "_" + avg_method + self.dataset_name)
                except Exception as e:
                    print(e)
                    pass

        # roc_auc_score
        if self.y_pred_proba is not None:
            # reshape prediction probability
            y_pred_proba_t = np.transpose(np.array(self.y_pred_proba)[:, :, 1])

            # roc_auc_score for multilabel
            for avg_method in ["micro", "macro", "weighted", "samples"]:
                try:
                    self.metrics_value.append(
                        round(metrics.roc_auc_score(self.y_true, y_pred_proba_t,
                                                    average=avg_method), 4))
                    self.metrics_name.append(self.get_name(
                        metrics.roc_auc_score) + "_" + avg_method +
                                             self.dataset_name)

                except Exception as e:
                    print(e)
                    pass

        # Multilabel ranking metrics using y_pred and y_pred_proba
        if self.y_pred_proba is not None:
            ranking_metrics = [metrics.coverage_error,
                               metrics.label_ranking_average_precision_score,
                               metrics.label_ranking_loss,
                               metrics.ndcg_score]

            self.populate_metrics_yproba(ranking_metrics)

    def plot_precision_recall_curve_binary(self):
        """

        Plot precision recall curve for binary classifiers

        """
        try:
            precision = dict()
            recall = dict()
            average_precision = dict()
            # for i in range(n_classes):
            i = 1
            precision[i], recall[i], _ = sklearn.metrics.precision_recall_curve(
                self.y_true,
                self.y_pred_proba[:, i])
            average_precision[i] = sklearn.metrics.average_precision_score(
                self.y_true, self.y_pred_proba[:, i])

            plt.figure()
            plt.step(recall[i], precision[i], where='post')
            plt.xlabel('Recall')
            plt.ylabel('Precision')
            plt.ylim([0.0, 1.05])
            plt.xlim([0.0, 1.0])
            plt.title(
                'Precision-Recall Curve' + self.dataset_name +
                ': Average Precision={0:0.2f}'.format(average_precision[i]))
            plt.show()
            xpresso_save_plot("Precision_recall_curve" + self.dataset_name,
                              output_path=MOUNT_PATH,
                              output_folder="report_metrics/" +
                                            self.status_desc)

        except Exception as e:
            print(e)
            raise

    def plot_precision_recall_curve_multi(self):
        """
        Plot precision recall curve for multiclass and multilabel classifiers

        """
        try:
            precision = dict()
            recall = dict()
            average_precision = dict()

            y_true_binarized = sklearn.preprocessing.label_binarize(self.y_true,
                                                                    classes=self.model_classes)

            for i in range(len(self.model_classes)):
                precision[i], recall[
                    i], _ = sklearn.metrics.precision_recall_curve(
                    y_true_binarized[:, i],
                    self.y_pred_proba[:, i])
                average_precision[i] = sklearn.metrics.average_precision_score(
                    y_true_binarized[:, i],
                    self.y_pred_proba[:, i])

            # A "micro-average": quantifying score on all classes jointly
            precision["micro"], recall[
                "micro"], _ = sklearn.metrics.precision_recall_curve(
                y_true_binarized.ravel(),
                self.y_pred_proba.ravel())
            average_precision[
                "micro"] = sklearn.metrics.average_precision_score(
                y_true_binarized, self.y_pred_proba,
                average="micro")
            # Plot all precision_recall curves
            plt.figure(figsize=(12, 6))

            colors = cycle([i for i in mcolors.TABLEAU_COLORS.values()])
            lines = []
            labels = []
            l, = plt.plot(recall["micro"], precision["micro"], color='gold',
                          lw=2)
            lines.append(l)
            labels.append('micro-average Precision-recall (area = {0:0.2f})'
                          ''.format(average_precision["micro"]))
            for i, color in zip(range(len(self.model_classes)), colors):
                l, = plt.plot(recall[i], precision[i], color=color, lw=2)
                lines.append(l)
                labels.append('Precision-recall for class {0} (area = {1:0.2f})'
                              ''.format(i, average_precision[i]))
            fig = plt.gcf()
            fig.subplots_adjust(bottom=0.25)
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel('Recall')
            plt.ylabel('Precision')
            plt.title(
                'Precision-Recall curve' + self.dataset_name + ': micro AP={'
                                                               '0:0.2f}'.format(
                    average_precision["micro"]))
            plt.legend(lines, labels, loc="lower left", prop=dict(size=9))
            plt.show()
            xpresso_save_plot("Precision_recall_curve" + self.dataset_name,
                              output_path=MOUNT_PATH,
                              output_folder="report_metrics/" +
                                            self.status_desc)

        except Exception as e:
            print(e)
            pass

    def plot_confusion_matrix(self):
        """
        Plot confusion matrix for binary or multiclass classifiers

        """
        # plot confusion matrix for binary or multiclass classification task
        try:
            cm = sklearn.metrics.confusion_matrix(self.y_true, self.y_pred)
            accuracy = np.trace(cm) / float(np.sum(cm))
            cmap = plt.get_cmap('Blues')

            plt.figure(figsize=(8, 6))
            plt.imshow(cm, interpolation='nearest', cmap=cmap)
            plt.colorbar()
            if self.model_classes is not None:
                tick_marks = np.arange(len(self.model_classes))
                plt.xticks(tick_marks, self.model_classes)
                plt.yticks(tick_marks, self.model_classes)
            thresh = cm.max() / 2
            for i, j in itertools.product(range(cm.shape[0]),
                                          range(cm.shape[1])):
                plt.text(j, i, "{:,}".format(cm[i, j]),
                         horizontalalignment="center",
                         color="white" if cm[i, j] > thresh else "black")

            plt.tight_layout()
            plt.ylabel('True label')
            plt.xlabel('Predicted label\naccuracy={:0.4f}'.format(accuracy))
            plt.title("Confusion Matrix" + self.dataset_name)
            plt.show()
            xpresso_save_plot("Confusion_matrix" + self.dataset_name,
                              output_path=MOUNT_PATH,
                              output_folder="report_metrics/" +
                                            self.status_desc)
            logger.info("Confusion Matrix Saved")
        except Exception as e:
            print(e)
            raise

    def plot_multilabel_confusion_matrix(self):
        """

        Plot confusion matrix for mulitlabel classifiers

        """
        try:
            # generate multilabel_confusion_matrix
            cm_arrays = metrics.multilabel_confusion_matrix(self.y_true,
                                                            self.y_pred)

            fig, axes = plt.subplots(len(cm_arrays), figsize=(6, 10))

            # plot a confusion matrix for each class
            for i in range(len(cm_arrays)):
                df_cm = pd.DataFrame(cm_arrays[i])
                sn.set(font_scale=1.2)  # for label size
                sn.heatmap(df_cm, annot=True,
                           cmap=plt.cm.Blues, ax=axes[i], fmt='g')
                cm_plot_title = self.get_name(
                    metrics.multilabel_confusion_matrix) + \
                                self.dataset_name + "_class_" + str(
                    self.model_classes[i])
                axes[i].set_xlabel('Predicted label')
                axes[i].set_ylabel('True la'
                                   'bel')
                axes[i].set_title(cm_plot_title)

            fig.tight_layout(pad=1.0)
            plt.show()
            xpresso_save_plot(cm_plot_title, output_path=MOUNT_PATH,
                              output_folder="report_metrics/" +
                                            self.status_desc)
            logger.info("Multilable Confusion Matrix Saved")
        except Exception as e:
            print(e)
            pass

    def plot_roc_curve_binary(self):
        """
        Plot ROC curve and compute ROC area for binary classifiers

        """
        try:
            average_roc_auc_score = metrics.roc_auc_score(self.y_true,
                                                          self.y_pred_proba[:,
                                                          1])
            fpr = dict()
            tpr = dict()
            roc_auc = dict()
            i = 1
            fpr[i], tpr[i], _ = metrics.roc_curve(self.y_true,
                                                  self.y_pred_proba[:, i])
            roc_auc[i] = metrics.auc(fpr[i], tpr[i])

            plt.figure()

            lw = 1
            plt.plot(fpr[lw], tpr[lw], color='darkorange',
                     lw=lw, label='ROC curve (area = %0.2f)' % roc_auc[lw])
            plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel('False Positive Rate')
            plt.ylabel('True Positive Rate')
            plt.title('ROC curve' + self.dataset_name +
                      ' : AUC ROC={0:0.2f}'.format(average_roc_auc_score))
            plt.legend(loc="lower right")
            plt.show()
            xpresso_save_plot("ROC_curve", output_path=MOUNT_PATH,
                              output_folder="report_metrics/" +
                                            self.status_desc)
        except Exception as e:
            print(e)
            pass

    def plot_roc_curve_multi(self):
        """

        Plot ROC curve and compute ROC area for multiclass and multilabel
        classifiers

        """
        # Compute ROC curve and ROC area for multiclass and multilabel
        # classifiers
        if self.y_pred_proba is not None:

            try:
                y_true_binarized = label_binarize(self.y_true,
                                                  classes=self.model_classes)
                fpr = dict()
                tpr = dict()
                roc_auc = dict()
                for i in range(len(self.model_classes)):
                    fpr[i], tpr[i], _ = metrics.roc_curve(
                        y_true_binarized[:, i], self.y_pred_proba[:, i])
                    roc_auc[i] = metrics.auc(fpr[i], tpr[i])

                # Compute micro-average ROC curve and ROC area
                fpr["micro"], tpr["micro"], _ = metrics.roc_curve(
                    y_true_binarized.ravel(), self.y_pred_proba.ravel())
                roc_auc["micro"] = metrics.auc(fpr["micro"], tpr["micro"])

                # First aggregate all false positive rates
                all_fpr = np.unique(np.concatenate(
                    [fpr[i] for i in range(len(self.model_classes))]))

                # Then interpolate all ROC curves at this points
                mean_tpr = np.zeros_like(all_fpr)
                for i in range(len(self.model_classes)):
                    mean_tpr += np.interp(all_fpr, fpr[i], tpr[i])

                # Finally average it and compute AUC
                mean_tpr /= len(self.model_classes)

                fpr["macro"] = all_fpr
                tpr["macro"] = mean_tpr
                roc_auc["macro"] = metrics.auc(fpr["macro"], tpr["macro"])

                # Plot all ROC curves
                plt.figure(figsize=(12, 6))
                lw = 2
                plt.plot(fpr["micro"], tpr["micro"],
                         label='micro-average ROC curve (area = {0:0.2f})'
                               ''.format(roc_auc["micro"]),
                         color='deeppink', linestyle=':', linewidth=4)

                plt.plot(fpr["macro"], tpr["macro"],
                         label='macro-average ROC curve (area = {0:0.2f})'
                               ''.format(roc_auc["macro"]),
                         color='navy', linestyle=':', linewidth=4)

                # create different colors for each class
                colors = cycle([i for i in mcolors.TABLEAU_COLORS.values()])
                for i, color in zip(range(len(self.model_classes)), colors):
                    plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                             label='ROC curve of class {0} (area = {1:0.2f})'
                                   ''.format(i, roc_auc[i]))

                plt.plot([0, 1], [0, 1], 'k--', lw=lw)
                plt.xlim([0.0, 1.0])
                plt.ylim([0.0, 1.05])
                plt.xlabel('False Positive Rate')
                plt.ylabel('True Positive Rate')
                roc_plot_title = self.model.__class__.__name__ + '_ROC_curve' \
                                 + self.dataset_name
                plt.title(roc_plot_title)
                plt.legend(loc="lower right")
                plt.show()
                xpresso_save_plot(roc_plot_title, output_path=MOUNT_PATH,
                                  output_folder="report_metrics/" +
                                                self.status_desc)
                logger.info("Multiclass/Multilable ROC Curve Saved")
            except Exception as e:
                print(e)
                pass

    def save_classification_report(self):
        """

        Save classification report (as csv file) for classification models

        """

        try:
            cls_report = metrics.classification_report(self.y_true, self.y_pred,
                                                       output_dict=True)
            cls_report_df = pd.DataFrame(cls_report).transpose()
            # save model to data folder
            pickle.dump(cls_report_df, open(
                os.path.join(MOUNT_PATH, "report_metrics/classification_report"
                             + self.dataset_name + ".pkl"), 'wb'))

            logger.info("Classification Report Saved")
        except Exception as e:
            print(e)
            pass

    def get_name(self, metric):
        """
        Reformat metrics name to make sure metric from different libraries to
        be consistent
        Args:
            metrics_name_str: string, metrics name

        Returns: reformatted metrics name

        """

        if "tensorflow.python.keras" in metric.__module__:
            metric_name = metric.__class__.__name__
            metrics_name_split = [w for w in
                                  re.split("([A-Z][^A-Z]*)", metric_name) if w]
            reformated_name = " ".join(metrics_name_split)
        else:
            # "sklearn" in metric.__module__ or custom metrics
            metric_name = metric.__name__
            reformated_name = self.reformat_metrics(metric_name)

        return reformated_name

    def reformat_metrics(self, metric_name):
        """

        Re-format metrics name to be more readable

        """
        removed_punct = metric_name.replace("_", " ")
        reformated_name = " ".join(
            [w.capitalize() for w in removed_punct.split()])
        return reformated_name


class LightgbmPlots:
    def __init__(self):
        self.logger = XprLogger(name="LightgbmPlots")

    def plot_learning_curve(self, model=None, evals_result=None):
        """
        Generate learning curves for a trained LightGBM. model: the test,
        train scores vs num_boost_round.

        Users can choose to pass in either a trained LightGBM model trained
        with Scikit-Learn API (lightgbm.LGBMRegressor
         or lightgbm.LGBMClassifier or the evaluation results (in dictionary)
         returned by training a LightGBM
        Booster model (trained with LightGBM Training API lightgbm.train()
        and parameter evals_result is specified)

        Parameters
        ----------
        estimator : a trained LightGBM model using Scikit-Learn API (
        LGBMModel, LGBMClassifier or LGBMRegressor)

        evals_result : a dictionary containing evaluation results returned
        from lightgbm.train()
                     (model is trained using LightGBM Training API
                     lightgbm.train() and parameter evals_result is
                     specified)

        """
        # if the trained model is a LGBMModel instance, pass in the model
        # if the model is Booster(lightgbm.train()), use the Dictionary
        # returned from lightgbm.train()

        if (model is not None) and (isinstance(model, lgb.sklearn.LGBMModel)):
            evals_result_dict = model.evals_result_
            plot_metric_booster = model
        if (evals_result is not None) and (isinstance(evals_result, dict)):
            evals_result_dict = evals_result
            plot_metric_booster = evals_result

        # identify evaluation metrics used when fitting the lightGBM model
        valid_sets_list = []
        e_mtr_list = []
        for val_sets, e_mtrs in evals_result_dict.items():
            # collect names of validation sets
            valid_sets_list.append(val_sets)
            for e_mtr_name, e_mtr_vals in e_mtrs.items():
                # collect name of evaluation metrics
                e_mtr_list.append(e_mtr_name)

        # Obtain the unique metrics
        metrics_set = np.unique(e_mtr_list)

        # plot training curve for each metrics
        for e_metric in metrics_set:
            try:
                lgb.plot_metric(plot_metric_booster, e_metric,
                                title=self.reformat_metrics(
                                    e_metric) + " during training")
                plt.show()
                xpresso_save_plot("learning_curve_" + e_metric,
                                  output_path=MOUNT_PATH,
                                  output_folder="report_metrics/" +
                                                self.status_desc)
                logger.info("Learning Curve Saved")

            except Exception:
                import traceback
                traceback.print_exc()
